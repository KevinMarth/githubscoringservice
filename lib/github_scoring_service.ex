defmodule GitHubScoringService do
  require Logger
  @moduledoc false
  @headers [
    {"Accept", "application/vnd.github.v3+json"},
    {"Authorization", "Basic S2V2aW5NYXJ0aDo5YjM3MmZhYTZlNmEzNDhjOGJjYWU4YzcwZjEyMWQ2Njc0Y2JjNDdj"},
  ]
  @options [recv_timeout: 1000*60]
  @pagination 30
  @state %{
    CreateEvent: 0,
    PullRequestReviewCommentEvent: 0,
    PushEvent:  0,
    WatchEvent: 0,
    OtherEvent: 0,
  }

  @doc """
  Compute the GitHub score for `user` based on public events.
  See https://developer.github.com/v3/activity/events/#list-public-events-performed-by-a-user
  """
  def score(user) when is_binary(user) do
    url = "https://api.github.com/users/#{user}/events/public"
    {:ok,agent} = Agent.start(fn -> @state end)
    1..event_page_count(url)
    |> Enum.map(&(Task.async(fn -> event_page(url, &1, agent) end)))
    |> Enum.map(&Task.await/1)
    state = Agent.get(agent, fn (state)-> state end)
    Map.put(state, :TotalScore, score(state))
  end

  def score(%{}=state) do
    state[:CreateEvent]*2 +
    state[:PullRequestReviewCommentEvent]*4 +
    state[:PushEvent]  *5 +
    state[:WatchEvent] *3 +
    state[:OtherEvent]
  end

  @doc """
  Count the pages of events at `url` using the `Link` header.
  See https://developer.github.com/v3/#pagination
  """
  def event_page_count(url) when is_binary(url) do
    params = [page: 1, per_page: @pagination]
    options = [params: params] ++ @options
    response = HTTPoison.head!(url, @headers, options)
    List.keyfind(response.headers, "Link", 0)
    |> event_page_count
  end

  def event_page_count(nil), do: 1
  def event_page_count(link) when is_tuple(link) do
    uri = elem(link,1)
    |> String.split(",", trim: true) |> Enum.at(1)
    |> String.split(";", trim: true) |> Enum.at(0)
    |> String.trim |> URI.parse
    query = uri.query |> URI.decode_query
    String.to_integer(query["page"])
  end

  @doc """
  Count the events on a `page` at `url`.
  See https://developer.github.com/v3/activity/events/types
  """
  def event_page(url, page, agent) do
    Logger.debug("#{url}&page=#{page}")
    params = [page: page, per_page: @pagination]
    options = [params: params] ++ @options
    response = HTTPoison.get!(url, @headers, options)
    events = Poison.decode!(response.body)
    count = Enum.reduce(events, %{}, &event/2)
    Agent.update(agent, __MODULE__, :events_update, [count])
  end

  def events_update(%{}=state, %{}=count) do
    Map.merge(state, count, fn (_key, state, count)-> state+count end)
  end

  defp event(%{"type"=> "CreateEvent"}, %{}=map) do
    event(:CreateEvent, map)
  end

  defp event(%{"type"=> "PullRequestReviewCommentEvent"}, %{}=map) do
    event(:PullRequestReviewCommentEvent, map)
  end

  defp event(%{"type"=> "PushEvent"}, %{}=map) do
    event(:PushEvent, map)
  end

  defp event(%{"type"=> "WatchEvent"}, %{}=map) do
    event(:WatchEvent, map)
  end

  defp event(%{"type"=> _type}, %{}=map) do
    event(:OtherEvent, map)
  end

  defp event(key, %{}=map) when is_atom(key) do
    Map.put(map, key, (map[key] || 0) +1)
  end
end

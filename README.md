# GitHub Scoring Service

## About

In this project we will be computing "score cards" for GitHub users based on a stream of events and providing a route to surface this data.

Events and their importance (represented by a score) are presented here:

| Event Type | Points |
|------------|---|
| PushEvent  | 5 |
| PullRequestReviewCommentEvent | 4 |
| WatchEvent  | 3 |
| CreateEvent | 2 |
| Every other event | 1 |

## Usage

It is assumed that Erlang and Elixir are installed on the host server. The service has been tested on Ubuntu-LTS-16 and macOS Sierra.
The following steps will install the service on the host server. For a macOS install, as documented at https://github.com/benoitc/hackney/issues/387,
it may be necessary to edit `mix.exs` and add the dependency `{:hackney, "1.6.1", override: true },` before running `mix deps.get`.

  * `git clone https://KevinMarth@bitbucket.org/KevinMarth/githubscoringservice.git GitHubScoringService`
  * `cd GitHubScoringService`
  * install dependencies with `mix deps.get`
  * start Phoenix endpoint with `mix phx.server`

Now visit [`localhost:4000/api`](http://localhost:4000/api) from your browser to use the Phoenix Swagger interface to the service.

## Notes

Several points are of note in the implementation of the server.

- The business logic (the logic to compute a GitHub score) is cleanly and completely separated from the web interface.
- Both the business logic and the web interface have tests (under the `test` directory). The business logic tests use a `mock` setup.
- The event stream for a user is processed in parallel. Each page of events is fetched and processed by a dedicated parallel task.
- The GitHub REST API implements rate limiting. Basic authentication has been implemented to enable throughput of 5000 GitHub requests per hour.

defmodule GitHubScoringServiceWeb.View do
  use GitHubScoringServiceWeb, :view

  def render("object.json", %{object: object}) do
    object
  end
end

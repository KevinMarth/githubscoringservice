# Usage

Execute the following steps to compute the GitHub score for a specified `user`.

- click on `GET`
- click on `Try it out`
- specify the `user` (the GitHub username, e.g. `elixir-lang`)
- click on `Execute`
- scroll down to see the `Server response`

The `Response body` is a JSON object with the count of each event type and the `TotalScore` for the `user`.

The service can also be used from the shell.

```
$ curl -X GET "http://localhost:4000/api/scores/elixir-lang" -H "accept: application/json"
```

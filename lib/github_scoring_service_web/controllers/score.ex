defmodule GitHubScoringServiceWeb.Controller.Score do
  use GitHubScoringServiceWeb, :controller
  use PhoenixSwagger
  alias GitHubScoringService,    as: Service
  alias GitHubScoringServiceWeb, as: Web

  def show(connection, %{"user"=> user}) do
    object = Service.score(user)
    connection |> render(Web.View, "object.json", object: object)
  end

  swagger_path(:show) do
    get "/api/scores/{user}"
    tag "Score"
    summary "Get Score for User"
    produces "application/json"
    parameter :user, :path, :string, "GitHub User", required: true
    response 200, "Success"
  end
end

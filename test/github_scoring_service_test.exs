defmodule GitHubScoringServiceTest do
  use ExUnit.Case, async: false
  import Mock
  alias GitHubScoringService, as: Service

  @url "https://api.github.com/user/events/public"
  @options [recv_timeout: 1000*60]
  @pagination 30
  @count %{
    CreateEvent: 2, PushEvent: 3, WatchEvent: 2, OtherEvent: 2,
    PullRequestReviewCommentEvent: 1,
  }

  defp response(@url) do
    body = """
    [{"type": "CreateEvent"}, {"type": "CreateEvent"}, {"type": "DeleteEvent"},
     {"type": "PushEvent"},   {"type": "PushEvent"},   {"type": "PushEvent"},
     {"type": "WatchEvent"},  {"type": "WatchEvent"},  {"type": "ForkEvent"},
     {"type": "PullRequestReviewCommentEvent"}]
    """
    %HTTPoison.Response{body: body}
  end

  describe "Scoring Service" do
    test "event page count" do
      link = {"Link", """
      <https://api.github.com/user/events/public?page=3&per_page=30>; rel="next",
      <https://api.github.com/user/events/public?page=5&per_page=30>; rel="last"
      """
      }
      assert Service.event_page_count(link) == 5
    end
    test "event page" do
      url = @url
      page = 5
      params  = [page: page, per_page: @pagination]
      options = [params: params] ++ @options
      with_mock HTTPoison, [get!: fn (^url, _headers, ^options)-> response(url) end] do
        {:ok,agent} = Agent.start(fn -> %{} end)
        assert Service.event_page(@url, page, agent) == :ok
        assert Agent.get(agent, fn (state)-> state end) == @count
      end
    end
    test "score" do
      create = 2*2
      push   = 3*5
      watch  = 2*3
      other  = 2*1
      assert Service.score(@count) == create + 4 + push + watch + other
    end
  end
end

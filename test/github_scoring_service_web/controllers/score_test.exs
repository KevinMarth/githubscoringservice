defmodule GitHubScoringServiceWeb.Controller.ScoreTest do
  use GitHubScoringServiceWeb.ConnCase

  describe "Score" do
    test "show 200", %{conn: connection} do
      response = connection
      |> get(score_path(connection, :show, "elixir-lang"))
      |> json_response(200)
      assert response["TotalScore"] >= 0
    end
  end
end

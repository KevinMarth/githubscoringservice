defmodule GitHubScoringServiceWeb.Router do
  use GitHubScoringServiceWeb, :router
  use PhoenixSwagger

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", GitHubScoringServiceWeb.Controller do
    pipe_through :api
    get "/scores/:user", Score, :show
  end

  scope "/api" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :github_scoring_service, swagger_file: "swagger.json", disable_validator: true
  end

  def swagger_info do
    {:ok, description} = File.read("docs/description.md")
    %{
      info: %{
        version: "0.0.1",
        title: "GitHub Scoring Service",
        description: description,
      },
      tags: [
        %{name: "Score"},
      ]
    }
  end
end
